﻿using System.Text.RegularExpressions;

namespace MockingHelper.TextAdaptionHelper
{
    public class UsingDirectivesAdapter : IUsingDirectivesAdapter
    {
        private const string USING = "using";
        private const string NUNIT = "NUnit.Framework";
        private const string MOQ = "Moq";

        public bool IsNunitUsingDirectiveAvailible(string text) =>
            IsUsingDirectiveAvailible(text, NUNIT);

        public bool IsMoqUsingDirectiveAvailible(string text) =>
            IsUsingDirectiveAvailible(text, MOQ);

        private bool IsUsingDirectiveAvailible(string text, string @namespace)
        {
            string pattern = $@"{USING}\s+{@namespace}\s*;";
            return Regex.Match(text, pattern).Success;
        }

        public string AddNunitUsingDirective(string text) =>
            Combine($"{USING} {NUNIT};\r\n", text);

        public string AddMoqUsingDirective(string text) =>
            Combine($"{USING} {MOQ};\r\n", text);

        private string Combine(string x, string y) =>
            string.Concat(x, y);
    }
}
