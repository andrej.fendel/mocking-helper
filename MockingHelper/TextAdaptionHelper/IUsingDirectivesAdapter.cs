﻿namespace MockingHelper.TextAdaptionHelper
{
    public interface IUsingDirectivesAdapter
    {
        string AddMoqUsingDirective(string text);
        string AddNunitUsingDirective(string text);
        bool IsMoqUsingDirectiveAvailible(string text);
        bool IsNunitUsingDirectiveAvailible(string text);
    }
}