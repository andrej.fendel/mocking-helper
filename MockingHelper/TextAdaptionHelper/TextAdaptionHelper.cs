﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace MockingHelper.TextAdaptionHelper
{
    public class TextAdaptionHelper : ITextAdaptionHelper
    {
        private readonly IUsingDirectivesAdapter usingDirectivesAdapter;

        public TextAdaptionHelper(IUsingDirectivesAdapter usingDirectivesAdapter) 
        {
            this.usingDirectivesAdapter = usingDirectivesAdapter;
        }
        public string Adapt(string fileCodeText)
        {
            if (fileCodeText == null)
                throw new ArgumentNullException(nameof(fileCodeText));

            var fieldStatmentInfos = ExtractFieldStatmentInfos(fileCodeText);

            if (fieldStatmentInfos.Any() == false)
                return fileCodeText;

            var newFieldsText = GetAdaptedFieldsText(fieldStatmentInfos);
            var setupMethodBodyStatementsText = GetSetupMethodBodyStatementsText(fieldStatmentInfos);
            var replacementText = CreateReplacementText(newFieldsText, setupMethodBodyStatementsText);
            var newText = Replace(fileCodeText, replacementText, fieldStatmentInfos);

            if (IsNunitUsingDirectiveAvailible(newText) == false)
                newText = AddNunitUsingDirective(newText);

            if (IsMoqUsingDirectiveAvailible(newText) == false)
                newText = AddMoqUsingDirective(newText);

            return newText;
        }

        private IEnumerable<FieldStatmentInfo> ExtractFieldStatmentInfos(string text)
        {
            string pattern = @"private\s+readonly\s+(\w+)\s+(\w+);";
            var matches = Regex
                .Matches(text, pattern)
                .OfType<Match>();

            return matches.Select(m => new FieldStatmentInfo(m, text));
        }

        private string GetAdaptedFieldsText(IEnumerable<FieldStatmentInfo> fieldStatmentInfos)
        {
            return fieldStatmentInfos
                .Select(m => $"private Mock<{m.Type}> {m.Identifier}Mock;{m.SpaceFromMargin}")
                .Aggregate((result, next) => result + next);
        }

        private string GetSetupMethodBodyStatementsText(IEnumerable<FieldStatmentInfo> fieldStatmentInfos)
        {
            return fieldStatmentInfos
                .Select(m => $"{m.Identifier}Mock = new Mock<{m.Type}>(MockBehavior.Strict);{m.SpaceFromMargin}")
                .Aggregate((result, next) => result + GetBlankSpaces(4) + next);
        }

        private string GetBlankSpaces(int count) 
        {
            if (count < 0)
                throw new ArgumentException($"{nameof(count)} must be greater than zero.");

            string GetBlankSpacesImpl(int c)
            {
                if (c== 0)
                    return string.Empty;

                return string.Concat(GetBlankSpacesImpl(c - 1), " ");
            }

            return GetBlankSpacesImpl(count);
        }



        private string CreateReplacementText(
            string fields,
            string setupMethodStatment)
        {
            return $@"{fields}

        [SetUp]
        public void Setup()
        {{
            {setupMethodStatment}
        }}";
        
        }

        private string Replace(
            string input, 
            string replacement,
            IEnumerable<FieldStatmentInfo> fieldStatmentInfos)
        {
            string pattern = fieldStatmentInfos
                .Select(m => m.MatchedText)
                .Aggregate((result, next) => $@"{result}\s*{next}");

            return Regex.Replace(input, pattern, replacement);
        }

        private bool IsNunitUsingDirectiveAvailible(string text) =>
            usingDirectivesAdapter.IsNunitUsingDirectiveAvailible(text);

        private bool IsMoqUsingDirectiveAvailible(string text) =>
            usingDirectivesAdapter.IsMoqUsingDirectiveAvailible(text);

        private string AddNunitUsingDirective(string text) =>
            usingDirectivesAdapter.AddNunitUsingDirective(text);

        private string AddMoqUsingDirective(string text) =>
            usingDirectivesAdapter.AddMoqUsingDirective(text);
    }
}
