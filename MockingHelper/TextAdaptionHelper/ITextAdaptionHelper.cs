﻿namespace MockingHelper.TextAdaptionHelper
{
    public interface ITextAdaptionHelper
    {
        string Adapt(string text);
    }
}