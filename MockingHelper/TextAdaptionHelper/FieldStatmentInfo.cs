﻿using System.Text.RegularExpressions;

namespace MockingHelper.TextAdaptionHelper
{
    internal class FieldStatmentInfo
    {
        private readonly Match match;
        private readonly string text;

        internal FieldStatmentInfo(
            Match match,
            string text)
        {
            this.match = match;
            this.text = text;
        }

        internal string Type => this.match.Groups[1].Value;
        internal string Identifier => this.match.Groups[2].Value;
        internal string MatchedText => this.match.Value;
        internal string SpaceFromMargin => GetSpaceFromMargin();

        private string GetSpaceFromMargin()
        {
            string pattern = $@"(\s*){this.match.Value}";
            return Regex.Match(this.text, pattern).Groups[1].Value;
        }
    }
}
