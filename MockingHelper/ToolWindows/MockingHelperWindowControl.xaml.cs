﻿using EnvDTE;
using MockingHelper.TextAdaptionHelper;
using System.Diagnostics.CodeAnalysis;
using System.Windows;
using System.Windows.Controls;

namespace MockingHelper.ToolWindows
{
    /// <summary>
    /// Interaction logic for MockingHelperWindowControl.
    /// </summary>
    public partial class MockingHelperWindowControl : UserControl
    {
        private readonly MockingHelperWindowState state;

        /// <summary>
        /// Initializes a new instance of the <see cref="MockingHelperWindowControl"/> class.
        /// </summary>
        public MockingHelperWindowControl(MockingHelperWindowState state)
        {
            this.state = state;
            this.InitializeComponent();
        }

        /// <summary>
        /// Handles click on the button by displaying a message box.
        /// </summary>
        /// <param name="sender">The event sender.</param>
        /// <param name="e">The event args.</param>
        [SuppressMessage("Microsoft.Globalization", "CA1300:SpecifyMessageBoxOptions", Justification = "Sample code")]
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Default event handler naming pattern")]
        private void helpMeButton_Click(object sender, RoutedEventArgs e)
        {
            AdaptText();
        }

        private void AdaptText() 
        {            
            var currentText = GetCurrentTextFile();
            IUsingDirectivesAdapter usingDirectivesAdapter = new UsingDirectivesAdapter();
            ITextAdaptionHelper textAdaptionHelper = new TextAdaptionHelper.TextAdaptionHelper(usingDirectivesAdapter);
            var newText = textAdaptionHelper.Adapt(currentText);
            var document = GetDocument();

            Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();
            document.ReplacePattern(currentText, newText);
        }

        public string GetCurrentTextFile()
        {
            Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();
            TextDocument doc = GetDocument();
            var p = doc.StartPoint.CreateEditPoint();
            return p.GetText(doc.EndPoint);
        }

        private TextDocument GetDocument()
        {
            Microsoft.VisualStudio.Shell.ThreadHelper.ThrowIfNotOnUIThread();
            return (TextDocument)(state.DTE.ActiveDocument.Object("TextDocument"));
        }
    }
}