﻿using Microsoft.VisualStudio.Shell;
using System;
using System.Runtime.InteropServices;

namespace MockingHelper.ToolWindows
{
    /// <summary>
    /// This class implements the tool window exposed by this package and hosts a user control.
    /// </summary>
    /// <remarks>
    /// In Visual Studio tool windows are composed of a frame (implemented by the shell) and a pane,
    /// usually implemented by the package implementer.
    /// <para>
    /// This class derives from the ToolWindowPane class provided from the MPF in order to use its
    /// implementation of the IVsUIElementPane interface.
    /// </para>
    /// </remarks>
    [Guid(WindowGuidString)]
    public class MockingHelperWindow : ToolWindowPane
    {

        public const string WindowGuidString = "aa4e3991-5c05-4d3f-85ae-dbdab49c1ad7"; 
        public const string Title = "Mocking Helper";
        /// <summary>
        /// Initializes a new instance of the <see cref="MockingHelperWindow"/> class.
        /// </summary>
        public MockingHelperWindow(MockingHelperWindowState state) : base(null)
        {
            this.Caption = Title;     
            this.Content = new MockingHelperWindowControl(state);
        }
    }
}
