﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockingHelper.ToolWindows
{
    public class MockingHelperWindowState
    {
        public MockingHelperWindowState(EnvDTE80.DTE2 dte) =>
            DTE = dte;
        public EnvDTE80.DTE2 DTE { get;  }
    }
}
