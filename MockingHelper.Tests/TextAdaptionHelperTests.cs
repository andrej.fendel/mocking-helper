﻿using MockingHelper.TextAdaptionHelper;
using NUnit.Framework;
using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace MockingHelper
{
    [TestFixture]
    public class TextAdaptionHelperTests
    {
        private ITextAdaptionHelper textAdaptionHelper;

        [SetUp]
        public void Setup()
        {
            textAdaptionHelper = new TextAdaptionHelper.TextAdaptionHelper(
                new UsingDirectivesAdapter());
        }

        [TestCaseSource(typeof(TextCodeWithMatchingFieldStatementsCases))]
        public void Adapt_GoodCases_AdaptsTextsAsExpected(string text)
        {
            textAdaptionHelper.Adapt(text);

            var expected = @"using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace RegexReplace
{
    public class TestClass
    {
        private Mock<IAInterface1> aInterface1Mock;
        private Mock<IBInterface1> bInterface1Mock;
        private Mock<ICInterface1> cInterface1Mock;

        [SetUp]
        public void Setup()
        {
            aInterface1Mock = new Mock<IAInterface1>(MockBehavior.Strict);
            bInterface1Mock = new Mock<IBInterface1>(MockBehavior.Strict);
            cInterface1Mock = new Mock<ICInterface1>(MockBehavior.Strict);
        }
    }
}";
            var actual = textAdaptionHelper.Adapt(text);
            Assert.That(EqualsStringIgnoringWhitespace(actual, expected), Is.True);
        }

        private class TextCodeWithMatchingFieldStatementsCases : IEnumerable
        {
            private const string codeTextWithoutRequiredUsingDirectives = @"using System;
using System.Collections.Generic;
using System.Text;

namespace RegexReplace
{
    public class TestClass
    {
        private readonly IAInterface1 aInterface1;
        private readonly IBInterface1 bInterface1;
        private readonly ICInterface1 cInterface1;
    }
}";
            private const string requiredUsingDirectives = @"using Moq;
using NUnit.Framework;
";            
            public IEnumerator GetEnumerator()
            {
                yield return new object[] { codeTextWithoutRequiredUsingDirectives };
                yield return new object[] { string.Concat(requiredUsingDirectives, codeTextWithoutRequiredUsingDirectives) };
            }
        }

        [Test]
        public void Adapt_TextHasNothingToAdapt_InputTextIsNotChanged()
        {
            string text = @"using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace RegexReplace
{
    public class TestClass
    {

    }
}";
            var actual = textAdaptionHelper.Adapt(text);
            Assert.That(EqualsStringIgnoringWhitespace(actual, text), Is.True);
        }

        private bool EqualsStringIgnoringWhitespace(string x, string y)
        {
            string normalizedX = Regex.Replace(x, @"\s", "");
            string normalizedY = Regex.Replace(y, @"\s", "");

            return string.Equals(
                normalizedX,
                normalizedY,
                StringComparison.OrdinalIgnoreCase);
        }
    }
}
